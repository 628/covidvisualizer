import './style.scss';
import {CountUp} from "countup.js";
import {Chart, ChartDataSets} from "chart.js";

type DataPointType = 'positive'
    | 'negative'
    | 'pending'
    | 'hospitalizedCurrently'
    | 'hospitalizedCumulative'
    | 'inIcuCurrently'
    | 'inIcuCumulative'
    | 'onVentilatorCurrently'
    | 'onVentilatorCumulative'
    | 'recovered'
    | 'death'
    | 'hospitalized'
    | 'totalTestResults'
    | 'total'
    | 'posNeg'
    | 'deathIncrease'
    | 'hospitalizedIncrease'
    | 'negativeIncrease'
    | 'positiveIncrease'
    | 'totalTestResultsIncrease'

interface CovidData {
    date: number,
    states: number,
    positive: number,
    negative: number,
    pending: number,
    hospitalizedCurrently: number,
    hospitalizedCumulative: number,
    inIcuCurrently: number,
    inIcuCumulative: number,
    onVentilatorCurrently: number,
    onVentilatorCumulative: number,
    recovered: number,
    dateChecked: string,
    death: number,
    hospitalized: number,
    totalTestResults: number,
    lastModified: string,
    total: number,
    posNeg: number,
    deathIncrease: number,
    hospitalizedIncrease: number,
    negativeIncrease: number,
    positiveIncrease: number,
    totalTestResultsIncrease: number,
    hash: string,
}

// Faster than regex, but at what cost?
function numToDate(date: number): Date {
    return new Date(date % 10 ** 8 / 10 ** 4, date % 10 ** 4 / 10 ** 2 - 1, date % 10 ** 2);
}

fetch('https://api.covidtracking.com/v1/us/daily.json')
    .then(response => response.json())
    .then(json => {
        let covidData = json as CovidData[]

        setQuickData(covidData[0])

        barChart('dailyCases',
            'Daily Cases',
            'United States Daily Cases',
            covidData,
            'positiveIncrease',
            true)

        barChart('dailyDeaths',
            'Daily Deaths',
            'United States Daily Deaths',
            covidData,
            'deathIncrease',
            true)
    })

fetch('https://api.covidtracking.com/v1/states/mi/daily.json')
    .then(response => response.json())
    .then(json => {
        let covidData = json as CovidData[]

        barChart('michiganDailyCases',
            'Daily Cases',
            'Michigan Daily Cases',
            covidData,
            'positiveIncrease',
            true)

        barChart('michiganDailyDeaths',
            'Daily Deaths',
            'Michigan Daily Deaths',
            covidData,
            'deathIncrease',
            true)
    })

function setQuickData(latestData: CovidData) {
    const c = new CountUp('confirmedNum', latestData.positive)
    const d = new CountUp('deathNum', latestData.death)
    const t = new CountUp('totalTestNum', latestData.totalTestResults)

    if (!c.error && !d.error && !t.error) {
        const timeout = 350

        setTimeout(() => {
            c.start()
        }, 1 * timeout)
        setTimeout(() => {
            d.start()
        }, 2 * timeout)
        setTimeout(() => {
            t.start()
        }, 3 * timeout)
    } else {
        console.log("Error updating quick data.")
    }
}

function barChart(id: string,
                  shortTitle: string,
                  longTitle: string,
                  dataSource: CovidData[],
                  dataPoint: DataPointType,
                  includeRollingAverage: boolean): void {
    let dataArray: { x: Date, y: number }[] = [];
    dataSource.forEach(item => {
        let date = new Date(numToDate(item.date))
        let value = item[dataPoint]

        if (value <= 0) return

        dataArray.push({x: date, y: value})
    })

    let rollingAvg: { x: Date, y: number }[] = [];
    if (includeRollingAverage) {
        for (let i = 3; i < dataArray.length - 3; i++) {
            let item = dataArray[i]

            if (item.y == 0) {
                continue
            }

            rollingAvg.push(
                {
                    x: item.x,
                    y: Math.round(
                        dataArray.slice(i - 3, i + 4)
                            .map(el => el.y)
                            .reduce((acc, cur) => acc + cur) / 7
                    )
                })
        }
    }

    let sets: ChartDataSets[] = [{
        label: shortTitle,
        backgroundColor: 'rgba(240, 128, 128, 0.75)',
        borderColor: 'rgb(240, 128, 128)',
        borderWidth: 2,
        data: dataArray,
        type: 'bar',
        order: 2
    }]

    if (includeRollingAverage) {
        sets.push({
            label: 'Rolling Avg',
            borderColor: 'rgb(30, 136, 229)',
            backgroundColor: 'rgba(0, 0, 0, 0)',
            data: rollingAvg,
            type: 'line',
            order: 1
        })
    }

    new Chart(id, {
        data: {
            datasets: sets
        },

        options: {
            responsive: true,
            maintainAspectRatio: false,
            scales: {
                xAxes: [{
                    type: 'time',
                    distribution: 'series',
                    time: {
                        unit: 'day',
                        minUnit: 'day',
                        round: 'day',
                    },
                }],
                yAxes: [{
                    type: 'linear'
                }]
            },
            animation: {
                duration: 1500,
                easing: 'easeOutCirc'
            },
            title: {
                display: true,
                text: longTitle,
                fontSize: 18,
            }
        },
    })
}
