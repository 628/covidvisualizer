# COVID-19 Data Visualizer
Web-based visualizer for aggregate COVID-19 data.  
  
~~**https://covid.petrovski.cc**~~ The subdomain has been removed, a live version can be accessed at https://628.gitlab.io/covidvisualizer/

This project is no longer being maintained.

## Data Source
Aggregate data courtesy of [The Covid Tracking Project](https://covidtracking.com/).
